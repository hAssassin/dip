/** ***************************************************************************
 * AFR - Adaptive Frameless Rendering 										  *	
 * @author	Krupicka Vojtech, xkrupi06 										  *
 * @file    Vectors.h														  *	
 * @date	09. 11. 2011													  *
 * @note  	ISO 8859-2														  *
 * @brief   Includes all vectors. Shortcut only.							  *
 **************************************************************************** */
 

// Conditional compilation 
#ifndef _AFR_VECTORS_H
#define _AFR_VECTORS_H


// Including project libraries
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"


#endif // #ifndef _AFR_VECTORS_H


/** ***************************************************************************/
/** end of file Vectors.h													  */
/** ***************************************************************************/
